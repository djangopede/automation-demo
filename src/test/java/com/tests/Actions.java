package com.tests;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.service.local.AppiumDriverLocalService;

import org.junit.Assert;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import java.net.URL;

import static io.appium.java_client.touch.offset.PointOption.point;


/**
 * Creates driver instance with hardcoded parameters,
 * initiates pageobjects with current driver,
 */
public class Actions {

    private static final String SUBJECT = "Email from myself";
    private static final String EMAIL_TEXT = "1234567890";
    private static final String TO_ME = "to me";
    private static final String NO_TOAST = "There was no toast this time!";
    private static final String EMAIL_DELETED = "Email deleted!";


    private static String RECIPIENT;

    private AppiumDriverLocalService service;
    private AppiumDriver<AndroidElement> driver;
    private PageObjects pageObjects;

    public void runAndroidSession() throws Exception {

        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "8.1.0");
        capabilities.setCapability("deviceName", "OnePlus 5");
        capabilities.setCapability("appPackage", "com.google.android.gm");
        capabilities.setCapability("appActivity", "ConversationListActivityGmail");

        URL url = new URL("http://127.0.0.1:4723/wd/hub");

        driver = new AppiumDriver<>(url, capabilities);
        pageObjects = new PageObjects();

        PageFactory.initElements(new AppiumFieldDecorator(driver, AppiumFieldDecorator.DEFAULT_WAITING_TIMEOUT), pageObjects);
    }

    /**
     * "Tap in corner of the screen" TouchAction
     *
     * @param driver
     */
    public void tapIntheCorner(AppiumDriver driver) {
        TouchAction action = new TouchAction((AppiumDriver) driver);
        Dimension size = driver.manage().window().getSize();
        int tapping_point_h = (int) (size.getHeight() * 0.8);
        int tapping_point_w = (int) (size.getWidth() * 0.8);
        action.tap((point(tapping_point_w, tapping_point_h))).perform();
    }

    /**
     * Actions for the given steps
     */

    /**
     * Closes Gmail FTU
     */
    public void closeFTU() {
        pageObjects.nextButton.click();
    }

    /**
     * Logs into Gmail with current google account
     */
    public void logIn() {
        pageObjects.doneButton.click();
    }

    /**
     * Checks if conversation list is displayed
     */
    public void conversationListIsDisplayed() {
        pageObjects.mailToolBar.isDisplayed();
        pageObjects.compose_button.isDisplayed();
    }

    /**
     * Clicks on "Create new emal" button
     */
    public void clickNewEmailButton() {
        pageObjects.compose_button.click();
    }

    /**
     * Saves sender email to a String variable
     */
    public void getSenderEmail() {
        RECIPIENT = pageObjects.senderEmail.getText();
    }

    /**
     * Enters recipient email from a String variable
     */
    public void enterRecipient() {
        pageObjects.recipient.sendKeys(RECIPIENT);
        pageObjects.recipient.click();
    }

    /**
     * Enter email subject (Hardcoded)
     */
    public void enterSubject() {
        pageObjects.subject.sendKeys(SUBJECT);
    }

    /**
     * Enters email text (Hardcoded)
     */
    public void enterEmailText() {
        pageObjects.emailBody.sendKeys(EMAIL_TEXT);
    }

    /**
     * Clicks "Send email" button
     */
    public void clickSendButton() {
        pageObjects.sendButton.click();
    }

    /**
     * Opens last sent email from "Myself"
     */
    public void openLastSentEmail() {
        pageObjects.sentEmails.get(0).click();
        pageObjects.sentEmails.get(0).click();
    }

    /**
     * Checks if toast has appeared and closes it if needed
     */
    public void closeToastIfPresent() {
        try {
            pageObjects.cancelToastButton.click();
        } catch (Exception e) {
            System.out.println(NO_TOAST);
        }
    }

    /**
     * Checks last sent email text against given String (Hardcoded)
     */
    public void checkLastEmail() {
        Assert.assertEquals(TO_ME, pageObjects.sentByField.getText());
    }

    /**
     * Clicks "Delete email" button
     */
    public void clickDeleteButton() {
        pageObjects.deleteButton.click();
        System.out.println(EMAIL_DELETED);
    }

    /**
     * Performs tap action
     */
    public void tapScreen() {
        tapIntheCorner(driver);
    }

    /**
     * Checks if there are emails present sent from "Myself"
     *
     * @return boolean
     */
    public boolean isListEmpty() {
        if (pageObjects.sentEmails.isEmpty())
            return true;
        else
            return false;
    }

    /**
     * Kills the session
     */
    public void tearDown() {
        driver.quit();
    }
}