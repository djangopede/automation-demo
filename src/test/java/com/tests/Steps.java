package com.tests;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.junit.Assert;

/**
 * Step definitions for feature files
 */
public class Steps extends Actions {

    /**
     * Starts gmail application
     *
     * @throws Throwable
     */

    @Given("^user starts gmail app$")
    public void userStartsGmailApp() throws Throwable {
        runAndroidSession();
        Thread.sleep(2000);
    }

    /**
     * Closes welcome message and selects user email
     *
     * @throws Throwable
     */
    @When("^user logs into gmail app$")
    public void userLogsIntoGmailApp() throws Throwable {
        closeFTU();
        Thread.sleep(2000);
        logIn();
    }

    /**
     * Checks if compose button is visible hence the app is open
     *
     * @throws Throwable
     */
    @Then("^gmail app is started$")
    public void gmailAppIsOpened() throws Throwable {
        conversationListIsDisplayed();
        tearDown();
    }

    /**
     * Sends an email to the current account
     *
     * @throws Throwable
     */
    @When("^user sends an email$")
    public void userSendsAnEmail() throws Throwable {
        userLogsIntoGmailApp();
        clickNewEmailButton();
        getSenderEmail();
        enterRecipient();
        enterSubject();
        enterEmailText();
        clickSendButton();
    }

    /**
     * Checks if email is sent
     *
     * @throws Throwable
     */
    @Then("^email is sent$")
    public void emailIsSent() throws Throwable {
        gmailAppIsOpened();
        tearDown();
    }

    /**
     * Opens sent email, checks if this is the correct one
     *
     * @throws Throwable
     */
    @Then("^user opens sent email$")
    public void emailIsReceived() throws Throwable {
        Thread.sleep(3000);
        openLastSentEmail();
        Thread.sleep(4000);
        closeToastIfPresent();
        Thread.sleep(1000);
        checkLastEmail();
    }

    /**
     * User deletes all messages until there is none left
     *
     * @throws Throwable
     */
    @When("^user deletes all his messages$")
    public void userDeletesAllHisMessages() throws Throwable {
        Thread.sleep(2000);
        while (!isListEmpty()) {
            openLastSentEmail();
            Thread.sleep(3000);
            closeToastIfPresent();
            tapScreen();
            Thread.sleep(2000);
            clickDeleteButton();
            Thread.sleep(1000);

        }
    }

    /**
     * Checks if there are really no messages left, unnecessary but looks better
     *
     * @throws Throwable
     */
    @Then("^all messages are deleted$")
    public void allMessagesAreDeleted() throws Throwable {
        Assert.assertTrue(isListEmpty());
        tearDown();
    }
}