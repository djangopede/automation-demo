package com.tests;

import io.appium.java_client.android.AndroidElement;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Class that holds all PageObjects
 */
public class PageObjects extends Actions {

    @AndroidFindBy(id = "welcome_tour_got_it")
    public AndroidElement nextButton;

    @AndroidFindBy(id = "action_done")
    public AndroidElement doneButton;

    @AndroidFindBy(id = "mail_toolbar")
    public AndroidElement mailToolBar;

    @AndroidFindBy(id = "compose_button")
    public AndroidElement compose_button;

    @AndroidFindBy(id = "from_account_name")
    public AndroidElement senderEmail;

    @AndroidFindBy(id = "to")
    public AndroidElement recipient;

    @AndroidFindBy(id = "subject")
    public AndroidElement subject;

    @AndroidFindBy(id = "composearea_tap_trap_bottom")
    public AndroidElement emailBody;

    @AndroidFindBy(id = "send")
    public AndroidElement sendButton;

    @AndroidFindBy(xpath = "//android.view.View[contains(@content-desc,'Email from myself')]")
    List<WebElement> sentEmails;

    @AndroidFindBy(id = "button2")
    public AndroidElement cancelToastButton;

    @AndroidFindBy(id = "recipient_summary")
    public AndroidElement sentByField;

    @AndroidFindBy(id = "delete")
    public AndroidElement deleteButton;

    @AndroidFindBy(id = "conversation-footer")
    public AndroidElement footer;
}