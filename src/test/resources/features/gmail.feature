@gmail
Feature: gmail tests

  Scenario: user opens gmail app
    Given user starts gmail app
    When user logs into gmail app
    Then gmail app is started

  Scenario: user send an email
    Given user starts gmail app
    When user sends an email
    Then email is sent

  Scenario: user sends and opens email
    Given user starts gmail app
    When user sends an email
    Then user opens sent email

  Scenario: user deletes all emails
    Given user starts gmail app
    And user logs into gmail app
    When user deletes all his messages
    Then all messages are deleted
