# automation-demo
--------------------------------------------------------------------------------
# Preconditions:
* Windows PC is used
* Latest Appium version installed and server running (http://localhost:4723)
* One Plus 5 (8.1.0) present and connected to PC with google account present and connected to internet
* IntelliJ idea 2018 installed
* Java 8 installed
--------------------------------------------------------------------------------
# Steps:
```
1) Clone project
2) Run "build.gradle" to get all the dependencies (NB! Cucumber plugin is older because new does not work yet)
3) Run "gmail.feature" by right clicking and select "run build"
```
--------------------------------------------------------------------------------
